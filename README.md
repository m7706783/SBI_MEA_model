# SBI MEA Model
Here you will find the code and data needed to reproduce or expand upon the results in Doorn et al. 2024:
"Automated inference of disease mechanisms in patient-hiPSC-derived neuronal networks"
https://www.biorxiv.org/content/10.1101/2024.05.23.595522v1

All code uses the mackelab sbi package (https://github.com/sbi-dev/sbi) in a python 3.9 environment. 

This directory contains:
- The parameters (Simulations_modelparameters.csv) and resulting MEA features (Simulations_MEAfeatures.csv) of the 300,000 simulations used to train the NDE for the paper. Every row represents one simulation. In "Simulations_modelparameters", the columns in ascending order represent the parameters: 'noise', '$g_{Na}$', '$g_{K}$', '$g_{AHP}$', '$g_{AMPA}$', '$g_{NMDA}$', 'Conn%', r'$\tau_{D}$', 'U (STD)', 'U asyn'. In "Simulations_MEAfeatures", the columns in ascending order represent the MEA features: 'MFR', 'NBR', 'NBD', 'PSIB', '#FBs', 'CVIBI', 'mean CC', 'sd CC', 'mean ISI CC', 'sd ISI CC', 'ISI dist', 'mean ISI', 'sd ISI temp', 'sd isi elec', 'MAC'
- The "TrainedNDE": the neural density estimator trained using the 300,000 simulations in the mackelab sbi package. This can be loaded in python to evaluate and obtain the posterior distribution as described in 'FindPosterior.py".
- The "Simulator.py" contains the functions to perform simulations with the biophysical computational model of hiPSC-derived neuronal networks on MEA, and the function to extract the MEA features from either simulations or experimental data. For the simulations, the "elecranges.dat" is necessary. 
- "MakeFigures.py": contains some functions to construct images as used in:
- "FindPosterior.py": contains the code to construct images like in the paper, and to asses your own experimental data using the method described in the paper. Also three example experimental observations are provided example_observations: "SCN_WTC_2410", "SCN_DS_2410" and "SCN_GEFS_2410" to construct some images and try the code if your own data is not available. 

All experimental data can be requested from the original authors:
- Dravet Syndrome and GEFS+ data: https://academic.oup.com/brain/article/146/12/5153/7226643
- CACNA1A+/- data: https://academic.oup.com/brain/advance-article/doi/10.1093/brain/awae330/7832339
- Dynasore data is available here: https://gitlab.utwente.nl/m7706783/fb_model 
