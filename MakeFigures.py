import pickle
import scipy.io as sio
from brian2 import *

# specify where the images must be saved
outputdir = '../Figures/'


def rasterplot(APs, savetitle, dt, start, stop, color):
    figure(figsize=(8.5, 2.2), dpi=300)
    plt.plot(APs[:, 1] * dt, APs[:, 0], '|', markeredgecolor=color, ms=9, alpha=.3)
    plt.hlines([-0.3, 0.7, 1.7, 2.7, 3.7, 4.7, 5.7, 6.7, 7.7, 8.7, 9.7, 10.7], -3, stop / second + 1300,
               colors='black', linewidths=0.7)
    xlim([start/second, stop/second])
    axis("off")
    tight_layout()
    savefig(outputdir + savetitle + '.png', dpi=300, bbox_inches='tight')
    show()


def marginaldiffplot(samples1, samples2, numparams, priorlimits, parlabels, savetitle):
    for ind in range(numparams):
        fig, ax = plt.subplots(1, 1, figsize=(1.7, 1.7), dpi=300)
        vals, xvals = np.histogram(samples1[:, ind], bins=30)
        val_dist = xvals[1] - xvals[0]
        one, = ax.plot(xvals[1:] - val_dist / 2, vals, color='#EF6F6C')
        vals, xvals = np.histogram(samples2[:, ind], bins=30)
        two, = ax.plot(xvals[1:], vals, color='#9A98B5')
        ax.set_xlim(priorlimits[ind])
        ax.set_xlabel(parlabels[ind])
        ax.spines[['right', 'top']].set_visible(False)
        tight_layout()
        plt.savefig(outputdir + savetitle + parlabels[ind] + '.png', dpi=400, format='png',
                    bbox_inches="tight", transparent=True)
        plt.show()
