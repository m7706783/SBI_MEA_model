from brian2 import *
from numpy import *
from scipy import signal
import pickle
import torch
import sys
from scipy.signal import find_peaks
from scipy.fft import fft, fftfreq
from scipy.stats import norm
from itertools import combinations
import numpy as np


def MEAnetsimulate(parameter_set, simtime=165*second, transient=5*second):
    # Network parameters
    Nl = 10
    N = Nl * Nl                                         # number of neurons

    # Neuron parameters
    area = 300 * umetre ** 2                            
    Cm = (1 * ufarad * cm ** -2) * area                 # membrane capacitance
    E_l = -39.2 * mV                                    # nernst potential of leaky ions
    E_K = -80 * mV                                      # nernst potential of potassium
    E_Na = 70 * mV                                      # nernst potential of sodium
    gam_na = parameter_set[1].item()                    # modifies sodium channel conductance
    g_na = gam_na * (50 * msiemens * cm ** -2) * area   # maximal conductance of sodium channels
    gam_kd = parameter_set[2].item()                    # modifies potassium channel conductance
    g_kd = gam_kd * (5 * msiemens * cm ** -2) * area    # maximal conductance of delayed rectifyer potassium channels
    g_l = (0.3 * msiemens * cm ** -2) * area            # maximal leak conductance
    V_T = -30.4 * mV                                    # adapts firing threshold of neurons
    sigma = parameter_set[0].item() * mV                # standard deviation of the noisy voltage fluctuations

    # Adaptation parameters
    E_AHP = E_K                                         # nernst potential of afterhyperpolarization potassium current
    g_AHP = parameter_set[3].item() * nS                # maximal conductance of sAHP potassium channels
    tau_Ca = 6000 * ms                                  # recovery time constant sAHP channels
    alpha_Ca = 0.00035                                  # strength of the spike-frequency adaptation

    # synapse parameters
    g_ampa = parameter_set[4].item() * nS               # maximal conductance of AMPA channels
    g_nmda = parameter_set[5].item() * nS               # maximal conductance of NMDA channels
    E_ampa = 0 * mV                                     # nernst potentials of synaptic channels
    E_nmda = 0 * mV
    tau_ampa = 2 * ms                                   # recovery time constant of ampa conductance
    taus_nmda = 100 * ms                                # decay time constant of nmda conductance
    taux_nmda = 2 * ms                                  # rise time constant of nmda conductance
    alpha_nmda = 0.5 * kHz

    prob = parameter_set[6].item()                      # connection probability
    sd = 0.7                                            # SD of normal distribution of synaptic weights
    max_delay = 25 * ms                                 # maximum conduction delay between neurons furthest away

    tau_d = parameter_set[7].item() * ms                # Recovery time constant of synaptic depression (STD)
    U = parameter_set[8].item()                         # strength of STD

    tau_ar = 700 * ms                                   # time constant of asynchronous release
    U_ar = parameter_set[9].item()                      # strength of asynchronous release
    U_max = 0.5 / ms                                    # saturation level of asynchronous release
    x0 = 5                                              # number of neurotransmitters in a vesicle

    # BUILD NETWORK
    # neuron model
    eqs = Equations('''
    dV/dt = noise + (-g_l*(V-E_l)-g_na*(m*m*m)*h*(V-E_Na)-g_kd*(n*n*n*n)*(V-E_K)+I-I_syn+I_AHP)/Cm : volt
    dm/dt = alpha_m*(1-m)-beta_m*m : 1
    dh/dt = alpha_h*(1-h)-beta_h*h : 1
    dn/dt = (alpha_n*(1-n)-beta_n*n) : 1
    dhp/dt = 0.128*exp((17.*mV-V+V_T)/(18.*mV))/ms*(1.-hp)-4./(1+exp((30.*mV-V+V_T)/(5.*mV)))/ms*h : 1
    alpha_m = 0.32*(mV**-1)*4*mV/exprel((13*mV-V+V_T)/(4*mV))/ms : Hz
    beta_m = 0.28*(mV**-1)*5*mV/exprel((V-V_T-40*mV)/(5*mV))/ms : Hz
    alpha_h = 0.128*exp((17*mV-V+V_T)/(18*mV))/ms : Hz
    beta_h = 4./(1+exp((40*mV-V+V_T)/(5*mV)))/ms : Hz
    alpha_n = 0.032*(mV**-1)*5*mV/exprel((15*mV-V+V_T)/(5*mV))/ms : Hz
    beta_n = .5*exp((10*mV-V+V_T)/(40*mV))/ms : Hz
    noise = sigma*(2*g_l/Cm)**.5*randn()/sqrt(dt) :volt/second (constant over dt)
    I : amp
    I_syn =  I_ampa + I_nmda: amp
    I_ampa = g_ampa*(V-E_ampa)*(s_ampa) : amp
    I_nmda = g_nmda*(V-E_nmda)*(s_nmda_tot)/(1+exp(-0.062*V/mV)/3.57) : amp
    s_nmda_tot :1
    ds_ampa/dt = -s_ampa/tau_ampa + qar_tot :1 
    qar_tot :Hz
    I_AHP = -g_AHP*Ca*(V-E_AHP) : amp
    dCa/dt = - Ca / tau_Ca : 1 
    x : meter
    y : meter
    ''')

    # Make population of neurons
    P = NeuronGroup(N, model=eqs, threshold='V>0*mV', reset='Ca += alpha_Ca', refractory=2 * ms,
                    method='exponential_euler')

    # Initialize neuron parameters
    P.V = -39 * mV                                  # approximately resting membrane potential 
    P.I = '(rand() -0.5)* 19 * pA'                  # make neurons heterogeneously excitable

    # Position neurons on a grid
    grid_dist = 45 * umeter
    P.x = '(i % Nl) * grid_dist'
    P.y = '(i // Nl) * grid_dist'

    # synapse model
    eqs_synapsmodel = '''
    s_nmda_tot_post = w * s_nmda * x_d :1 (summed) 
    qar_tot_post = w * x0 * qar :Hz (summed)
    qar = clip(randn()*sqrt(x_d/x0*uar*dt*(1-uar*dt))+uar*dt*x_d/x0, 0, 2*x_d/x0*uar*dt)/dt :Hz (constant over dt)
    ds_nmda/dt = -s_nmda/(taus_nmda)+alpha_nmda*(x_nmda)*(1-s_nmda) + x0 * qar : 1 (clock-driven)
    dx_nmda/dt = -x_nmda/(taux_nmda) :1 (clock-driven)
    dx_d/dt = (1-x_d)/tau_d -qar :1 (clock-driven)
    duar/dt = -uar/tau_ar :Hz (clock-driven)
    w : 1
    '''
    eqs_onpre = '''
    x_nmda += 1 
    x_d *= (1-U) 
    uar += U_ar*(U_max-uar)
    s_ampa += w * x_d 
    '''
    # Make synapses
    Conn = Synapses(P, P, model=eqs_synapsmodel, on_pre=eqs_onpre, method='euler')
    Conn.connect(p=prob)
    Conn.w[:] = 'clip(1.+sd*randn(), 0, 2)'
    Conn.x_d[:] = 1
    V_max = (sqrt(Nl ** 2 + Nl ** 2) * grid_dist) / max_delay
    Conn.delay = '(sqrt((x_pre - x_post)**2 + (y_pre - y_post)**2))/V_max'

    # SET UP MONITORS AND RUN
    record_string = ['V']

    dt2 = defaultclock.dt                           # allows for changing the timestep of recording

    trace = StateMonitor(P, record_string, record=True, dt=dt2)
    spikes = SpikeMonitor(P)
    
    try:
        run(simtime, report='text', profile=True)   # run
    except:
        simulation_num = int(float(sys.argv[1]))
        torch.save([nan, nan, nan, nan, nan, nan, nan, nan], 'Results' + str(simulation_num) + '.pt')
        print("Simulation failed, results are set to nan and python will be terminated")
        exit()

    # Set up a filter to filter the voltage signal
    fs = 1 / (dt2 / second)                         # nyquist frequency
    fc = 100                                        # cut-off frequency of the high-pass filter
    w = fc / (fs / 2)                               # normalize the frequency
    b, a = signal.butter(5, w, 'high')
    voltagetraces = zeros((12, len(trace.t)))

    elec_grid_dist = (grid_dist * (Nl - 1)) / 4     # electrode grid size (there are 12 electrodes)
    elec_range = 3 * grid_dist                      # measurement range of each electrode
    comp_dist = ((Nl - 1) * grid_dist - elec_grid_dist * 3) / 2

    elecranges = pickle.load(open("elecranges.dat", "rb")) 

    k = 0
    max_APs = len(spikes.t)                         # maximum amount of detected APs
    APs = np.array([0, 0])
    for i in [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14]:
        templist = elecranges[i, :]
        x_electrode = i % 4 * elec_grid_dist + comp_dist
        y_electrode = i // 4 * elec_grid_dist + comp_dist
        templist = [j for j in templist if j != 0]
        voltage = np.zeros(len(trace.V[0]))
        for l in range(len(templist)):
            voltage += trace[templist[l]].V / mV * 1 / (
                    sqrt((x_electrode - P.x[templist[l]]) ** 2 + (y_electrode - P.y[templist[l]]) ** 2) / (grid_dist * 0.2))
        voltage = voltage - mean(voltage)
        voltagefilt = signal.filtfilt(b, a, voltage)  # high pass filter
        threshold = 4 * np.sqrt(np.mean(voltagefilt ** 2))  # threshold to detect APs
        APs_temp, _ = signal.find_peaks(abs(voltagefilt), height=threshold)
        for j in range(len(APs_temp)):
            APs = np.append(APs, [k, APs_temp[j]])
        templist = None
        k += 1

    APs = APs.reshape(len(APs) // 2, 2)

    return APs, simtime, transient, fs


def compute_features(APs, simtime, transient, fs):
    numelectrodes = 12                              # number of electrodes
    timbin = 25 * ms                                # timebin to compute network firing rate

    # Initialize
    APs_inbin = zeros((numelectrodes, int(floor((simtime - transient) / timbin))))
    spikerate = zeros((numelectrodes, int(floor((simtime - transient) / timbin))))

    # Delete transient
    APs_wot = APs[APs[:, 1] > transient * fs / second, :]
    APs_wot[:, 1] = APs_wot[:, 1] - transient * fs / second

    # Calculate the network firing rate
    for l in range(numelectrodes):
        APt = APs_wot[APs_wot[:, 0] == l, :]         # go through one electrode first
        APt = APt[:, 1]
        binsize = timbin * fs / second
        for k in range(int(floor((simtime - transient) / timbin))):
            APs_inbin[l, k] = sum((APt > (k * binsize)) & (APt < ((k + 1) * binsize)))
            spikerate[l, k] = APs_inbin[l, k] * second / timbin

    APs_inbintot = sum(APs_inbin, axis=0)
    spikeratetot = sum(spikerate, axis=0)

    # Smoothen the spikerate by convolution with gaussian kernel 
    width = 11                      
    sigma = 3.0
    x = np.arange(0, width, 1, float)
    x = x - width // 2
    kernel = norm.pdf(x, scale=sigma)
    kernel /= np.sum(kernel)
    spikeratesmooth = np.convolve(spikeratetot, kernel, mode='same')

    # Detect fragmented bursts on smoothed spikerate
    MB_th = (1/16) * max(spikeratetot)
    peaks, ph = find_peaks(spikeratesmooth, height=MB_th, prominence=(1 / 20) * max(spikeratetot))

    # Set parameters for burst detection
    act_elec = sum(mean(spikerate, axis=1) > 0.02)      # calculate the number of active electrodes

    start_th = 0.25 * max(spikeratetot)                 # spikerate threshold to start a burst
    t_th = int((50 * ms) / timbin)                      # how long it has to surpass threshold for to start burst
    e_th = 0.5 * act_elec                               # how many electrodes need to be active in the burst
    stop_th = (1 / 50) * max(spikeratetot)              # threshold to end a burst

    # Initialize burst detection
    i = 0
    NB_count = 0
    max_NBs = 1000                                      # maximum amount of to be detected bursts
    NBs = zeros((max_NBs, 4))

    # Detect NBs
    while (i + t_th) < len(spikeratetot):
        if (all(spikeratetot[i:i + t_th] > start_th)) \
                & (sum(sum(APs_inbin[:, i:i + t_th], axis=1) > t_th) > e_th):
            NBs[NB_count, 2] = NBs[NB_count, 2] + sum(APs_inbintot[i:i + t_th])
            NBs[NB_count, 0] = i
            i = i + t_th
            while any(spikeratetot[i:i + 2 * t_th] > stop_th):
                NBs[NB_count, 2] = NBs[NB_count, 2] + APs_inbintot[i]
                i = i + 1
            NBs[NB_count, 3] = sum((peaks > NBs[NB_count, 0]) & (peaks < i))
            NBs[NB_count, 1] = i
            NB_count = NB_count + 1
        else:
            i = i + 1

    NBs = NBs[0:NB_count, :]

    MNBR = NB_count * 60 * second / (simtime - transient)
    NBdurations = (array(NBs[:, 1]) - array(NBs[:, 0])) * timbin / second
    MNBD = mean(NBdurations)
    PSIB = sum(NBs[:, 2] / len(APs_wot)) * 100
    MFR = len(APs_wot) / ((simtime - transient) / second) / numelectrodes
    IBI = (array(NBs[1:, 0]) - array(NBs[0:-1, 1])) * timbin / second
    CVIBI = np.std(IBI) / np.mean(IBI)
    if NB_count == 0:
        MNBD = 0.0
        MNMBs = 0.0
        NFBs = 0
    else:
        NFBs = sum(NBs[:, 3]) / NB_count

    if NB_count < 2:
        CVIBI = 0.0

    # Calculate MAC metric as defined by Maheswaranathan
    yf = fft(spikeratetot)
    xf = fftfreq(len(spikeratetot), timbin / second)[:len(spikeratetot) // 2]
    MAC = max(np.abs(yf[1:len(spikeratetot)])) / np.abs(yf[0])

    # Calculate cross-correlation between binarized spike trains
    # Binarize the spike trains 
    all_combinations = list(combinations(list(arange(numelectrodes)), 2))
    trans_timebin = 0.2 * second                         # timebin to transform spiketrains to binary
    bin_timeseries = list(range(0, int((simtime - transient) / ms), int(trans_timebin / ms)))
    binary_signal = zeros((numelectrodes, len(bin_timeseries)))
    for i in range(numelectrodes):
        signal = APs_inbin[i, :]
        grosssignal = [sum(signal[x:x + int((trans_timebin / timbin))]) for x in
                       range(0, len(signal), int(trans_timebin / timbin))]
        binary_signal[i, :] = [1 if x > 0 else 0 for x in grosssignal]

    # Calculate coefficients between every pair of electrodes
    coefficients = []
    N = len(binary_signal[0, :])
    for i, j in all_combinations:
            signal1 = binary_signal[i, :]
            signal2 = binary_signal[j, :]
            if (i != j) & (not list(signal1) == list(signal2)):
                coefficients.append((N * sum(signal1 * signal2) - sum(signal1) * (sum(signal2)))
                                    * ((N * sum(signal1 ** 2) - sum(signal1) ** 2) ** (-0.5))
                                    * ((N * sum(signal2 ** 2) - sum(signal2) ** 2) ** (-0.5)))

    mean_corr = mean(coefficients)
    sd_corr = std(coefficients)

    if not coefficients:
        mean_corr = 1
        sd_corr = 0
        
    # Compute continuous ISI arrays
    time_vector = np.arange(0, (simtime - transient) / second, 1/fs)
    isi_arrays = np.zeros((numelectrodes, len(time_vector)))

    for electrode in range(numelectrodes):
        # Extract spike times for the current electrode
        electrode_spike_times = APs_wot[APs_wot[:, 0] == electrode, 1]

        for i in range(len(electrode_spike_times) - 1):
            spike1 = electrode_spike_times[i]
            spike2 = electrode_spike_times[i + 1]
            tisi = (spike2 - spike1) / fs

            # Fill ISI values in the appropriate range
            if i == 0:
                isi_arrays[electrode, 0:spike1] = NaN
            isi_arrays[electrode, spike1:spike2] = tisi
            if (i + 1) == (len(electrode_spike_times) - 1):
                isi_arrays[electrode, spike2:] = NaN

    # Compute ISI measures
    meanisi_array = np.nanmean(isi_arrays, axis=0)
    mean_ISI = np.nanmean(meanisi_array)
    sdmean_ISI = np.nanstd(meanisi_array)
    sdtime_ISI = np.nanmean(np.nanstd(isi_arrays, axis=0))

    # Calculate the ISI-distance and ISI correlations
    ISI_distances = np.zeros(len(all_combinations))
    isicoefficients = np.zeros(len(all_combinations))
    N = len(isi_arrays[0, :])
    j = 0
    
    # Iterate through the electrode combinations
    for electrode1_key, electrode2_key in all_combinations:
        # Get the ISI arrays for the selected electrodes
        isit1_wn = isi_arrays[electrode1_key, :]
        isit2_wn = isi_arrays[electrode2_key, :]
        isit1 = isit1_wn[~isnan(isit1_wn)]
        isit2_wn = isit2_wn[~isnan(isit1_wn)]
        isit2 = isit2_wn[~isnan(isit2_wn)]
        isit1 = isit1[~isnan(isit2_wn)]

        isi_diff = isit1 / isit2
        ISI_distances[j] = np.mean(np.where(isi_diff <= 1, abs(isi_diff - 1), -1 * (1 / isi_diff - 1)))

        if (i != j) & (not list(isit1) == list(isit2)):
            isicoefficients[j] = ((N * sum(isit1 * isit2) - sum(isit1) * (sum(isit2)))
                                  * ((N * sum(isit1 ** 2) - sum(isit1) ** 2) ** (-0.5))
                                  * ((N * sum(isit2 ** 2) - sum(isit2) ** 2) ** (-0.5)))

        j += 1

    mean_ISIcorr = mean(isicoefficients)
    sd_ISIcorr = std(isicoefficients)
    ISI_distance = np.mean(ISI_distances)

    return [MFR, MNBR, MNBD, PSIB, NFBs, CVIBI, mean_corr, sd_corr, mean_ISIcorr, sd_ISIcorr, ISI_distance, mean_ISI, sdmean_ISI, sdtime_ISI, MAC]


